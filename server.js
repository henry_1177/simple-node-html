const express = require('express')
const app = express()
const ejs = require('ejs')
const path = require('path')
const pretty = require('pretty')

// Templating engine define
app.set('view options', { layout: false })
app.engine('html', ejs.renderFile)
app.set('view engine', 'html')
app.set('views', __dirname + '/app')

// Disable caching
app.set('etag', false)
app.use((req, res, next) => {
    res.set('Cache-Control', 'no-store')
    next()
})

async function renderPage(page) {
    let content = ''
    await ejs.renderFile(
        `app/pages/${page}/index.html`,
        null,
        null,
        (err, str) => {
            content = str
        },
    )
    return content
}

// Routes
app.get('/', async (req, res) => {
    // Get html content
    let body = await renderPage('home')

    // Render data
    const date = new Date()
    const options = { body, year: date.getFullYear() }

    // Render html content and send to client
    res.render('index', options, (error, html) => {
        res.status(200).send(pretty(html, { ocd: true }))
    })
})

// Static file
app.use('/public', express.static(path.join(__dirname, 'public')))

var server = app.listen(8080, function() {
    var port = server.address().port

    console.log('App running at http://localhost:%s', port)
})
