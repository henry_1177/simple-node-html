# Simple Node HTML

A NodeJS server for simple HTML project

#### Begin

1. Clone this project
   `git clone git@gitlab.com:henry_1177/simple-node-html.git`
2. Go to project folder
   `cd simple-node-html`

3. Install dependencies in package.json
   `yarn install`

4. Start dev server
   `yarn start`
